# Recording Your Project

This documentation includes how to record your gameplay into Godot Engine.

![output2](https://gitlab.com/omerfaruktemiz/images-in-readme/uploads/bb6f52e1896557acef0778ce3613bb9e/output2.gif)

## Installing the recorder to your project

For recording, we use a plugin that save all frames as a ".png" and filing them into a folder named "out".
- Download or clone the plugin.
- Go to the plugin folder and copy "Recorder" folder which is into "addons", then paste it into your project's "addons" folder. After that, the folder tree of your project should seem like this:

![record_readme](https://gitlab.com/omerfaruktemiz/images-in-readme/uploads/4039a8930def805fec103a6015bc8b1c/record_readme.png)

- Enable the recorder plugin from "Project Settings > Plugins"

![record_readme_2](https://gitlab.com/omerfaruktemiz/images-in-readme/uploads/5b3906bb8e6669e4e65b94acee27d82f/record_readme_2.png)

## Installing FFMPEG

For creating a video from many png files, we use FFMPEG. For downloading and installing FFMPEG, you should look this link: https://www.youtube.com/watch?v=r1AtmY-RMyQ&ab_channel=TroubleChute

## Recording

For recording, you have to run your project. You can start recording with pressing "R" and you can stop recording with pressing "R" again. You can save the recorded frames with pressing "S". Saved frames are filed into "/out" folder (It'll be created if it doesn't exist) inside your projects folder.

After recording, "/out" folder should seem like below:

![recording_readme_3](https://gitlab.com/omerfaruktemiz/images-in-readme/uploads/f1ec4823722ca158acb23a324d89c10d/recording_readme_3.png)

To create a video from these png files, we use FFMPEG. Open the "cmd.exe" and go to your project directory. You can use 
- "cd <file_name>" (Goes to folder if <file_name> folder exists into current folder.)
- "cd.." (Returns to parent folder.) 
- dir (Lists the files into folder.)

commands for surfing between folders. You have to be into "/out" folder.

![frame6](https://gitlab.com/omerfaruktemiz/images-in-readme/uploads/5a04776be1d5ac4e48bce35293bd037c/frame6.png)

In "out" folder, we can create our video. For creating video, you have to run this line:
> ffmpeg -framerate 15 -i %03d.png -pix_fmt yuv420p -f mp4 videoname.mp4
- You can adjust the frame rate, 15 is recommended.
- You can change the video name.

After running this line, you can see the output video into "out" folder.

# References
Recorder plugin:  https://github.com/henriquelalves/GodotRecorder


